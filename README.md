# Spill Response

__Main author:__  Cole Fields  
__Affiliation:__  Fisheries and Oceans Canada (DFO)   
__Group:__        Marine Spatial Ecology and Analysis   
__Location:__     Institute of Ocean Sciences   
__Contact:__      e-mail:  | tel:


- [Objective](#objective)
- [Summary](#summary)
- [Status](#status)
- [Contents](#contents)
- [Methods](#methods)
- [Requirements](#requirements)
- [Caveats](#caveats)
- [Uncertainty](#uncertainty)
- [Acknowledgements](#acknowledgements)
- [References](#references)


## Objective
The purpose of this project is for document sharing with Environment Canada. 


## Summary
There is no code in this repository. The reason it is here is to make the project visible. 


## Status
In-development


## Contents
Documents supporting spatial files for NEEC.


## Methods
See supporting documents.


## Requirements
See supporting documents.


## Caveats
See supporting documents.


## Uncertainty
See supporting documents.


## Acknowledgements
See supporting documents.


## References
See supporting documents.
